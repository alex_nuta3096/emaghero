<?php
declare(strict_types=1);


/**
 * Interface SkillInterface
 * @package EmagiaStory\Skills
 */
namespace EmagiaStory\Skills;

interface SkillsInterface
{
    public function getSpecialDamage(int $damage): int;
}